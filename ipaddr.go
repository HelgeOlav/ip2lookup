package main

import (
	"github.com/ip2location/ip2location-go/v9"
	"sync"
)

// JSON key
const (
	countryCode = "country_code"
	countryName = "country_name"
	isp         = "ISP"
	cityName    = "city_name"
	regionName  = "region_name"
	location    = "location"
)

type ip2l struct {
	db       *ip2location.DB
	filename string
	dbMtx    sync.RWMutex
}

// newip2l creates a new struct from file
func newip2l(fn string) (*ip2l, error) {
	result := &ip2l{filename: fn}
	var err error
	result.db, err = ip2location.OpenDB(fn)
	return result, err
}

func (i *ip2l) Close() error {
	i.dbMtx.Lock()
	defer i.dbMtx.Unlock()
	return i.db.Close()
}
